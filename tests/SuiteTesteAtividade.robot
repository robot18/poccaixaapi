*** Settings ***
Resource         ${EXECDIR}/resources/Base.robot
Resource         ${EXECDIR}/resources/AtividadesGET.robot
Resource         ${EXECDIR}/resources/AtividadePOST.robot

Suite Setup      Conectar API

*** Test Cases ***

Realizar a requsição no GET Atividades
    Requisitar Activities - GET
    Conferir o status code    200
    Conferir o reason    OK
    Conferir se retorna uma lista com "30" atividades


Realizar a requsição no GET de uma Atividade
    Requisitar Atividade "11"
    #Requisitar Atividade "12"
    Conferir o status code    200
    Conferir o reason    OK

Realizar cadastro de uma Atividade
    Cadastrar uma nova atividade
    Conferir o status code   200
    Conferir o reason    OK
