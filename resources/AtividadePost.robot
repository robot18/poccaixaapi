*** Settings ***
Library    Collections
Library    RequestsLibrary
Library    BuiltIn

Resource         ${EXECDIR}/resources/Base.robot

# https://fakerestapi.azurewebsites.net/index.html

*** Variable ***

*** Keywords ***
Cadastrar uma nova atividade
    ${HEADERS}          Create Dictionary       content-type=application/json
    ${RESPOSTA}         POST On Session    API     Activities
    ...                 data={"id": 31,"title": "teste","dueDate": "2021-06-07T13:52:14.972Z","completed": true}
    ...                 headers=${HEADERS}
    Log                 ${RESPOSTA.text}
    Set Test Variable   ${RESPOSTA}
