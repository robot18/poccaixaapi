*** Settings ***
Library    Collections
Library    RequestsLibrary
Library    BuiltIn

# https://fakerestapi.azurewebsites.net/index.html

*** Variable ***
${URLAPI}       https://fakerestapi.azurewebsites.net/api/v1/

*** Keywords ***

Conectar API
    Create Session    API    ${URLAPI}

Conferir o status code
    [arguments]                   ${STATUSCODE_DESEJADO}
    Should Be Equal As Strings    ${RESPOSTA.status_code}   ${STATUSCODE_DESEJADO}

    #O parâmetro do código de razão ( Reason ) é uma qualificação para o parâmetro do código de conclusão ( CompCode ).
    #Se não houver razão especial a ser relatada, MQRC_NONE será retornado.
Conferir o reason
    [arguments]   ${REASON_DESEJADO}
    Log           ${RESPOSTA.reason}
    Should Be Equal As Strings    ${RESPOSTA.reason}    ${REASON_DESEJADO}
