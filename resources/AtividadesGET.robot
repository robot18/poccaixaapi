*** Settings ***
Library    Collections
Library    RequestsLibrary
Library    BuiltIn

Resource         ${EXECDIR}/resources/Base.robot

# https://fakerestapi.azurewebsites.net/index.html

*** Variable ***

*** Keywords ***

#Realizar a requisão de todas atividades
Requisitar Activities - GET
    ${RESPOSTA}          GET On Session    API    Activities
    Log                  ${RESPOSTA.json()}
    #Log                  ${RESPOSTA.text}
    #Log    ${RESPOSTA.content}
    Set Test Variable    ${RESPOSTA}

#Conferir o retorno se contém uma determinada quantidadeS
Conferir se retorna uma lista com "${QTDE_ACTIVITIES}" atividades
    #[arguments]         ${QTDE_ACTIVITIES}
    Length Should Be    ${RESPOSTA.json()}    ${QTDE_ACTIVITIES}

#Realizar a requisão de uma determinada atividade.
Requisitar Atividade "${ID_ATIVIDADE}"
    ${RESPOSTA}          GET On Session    API    Activities/${ID_ATIVIDADE}
    Log                  ${RESPOSTA.json()}
    Set Test Variable    ${RESPOSTA}
